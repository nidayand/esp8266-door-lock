# esp8266-door-lock

Arduino code for an NodeMCUv2 board that is controlling a door lock using an MG995 servo and a magnet switch

# Project purpose
- Control a door lock by using the existing lock in the door
- Use a servo to turn the lock
- Servo should only try to lock if the door is closed (magnetic sensor)
- In order to avoid the servo to break, it should return to an open state (unlocked) if there is too much resistance when locking the door
- Use MQTT messaging to control the state of the lock
- Report status via MQTT whenever a state has changed
- Disable servo power post state change in order to be able to use a key if necessary

# Implementation

# To do
- Disable power to servo post changed state
- Implement return to unlocked state if to much torque to turn servo
- MQTT message to reset device configuration - WIFI, MQTT server
- Button or reset-PIN to reset device configuration - WIFI, MQTT server
- Button to unlock if person is locked in
- Store preferences in SPIFFS