#include <Arduino.h>
#include <ESP8266WiFi.h>      //ESP8266 Core WiFi Library (you most likely already have this in your sketch)

#include <DNSServer.h>        //Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h> //Local WebServer used to serve the configuration portal
#include <WiFiManager.h>      //https://github.com/tzapu/WiFiManager WiFi Configuration Magic

#include <PubSubClient.h>     //MQTT client
#include <Servo.h>            //Servo client

// Board
static const uint8_t PIN_SERVO = D1;    //MG995 signal wire
static const uint8_t PIN_MAGNET = D5;   //Pull-up initiated

/*
Magnet sensor connected to D5 and GND
Server sensor connected to D1 (orange), Vin/5V (red), GND (brown)
Using servo MG995, NodeMCUv2 (esp8266-12E)

- If door is closed, it will lock upon initialization otherwise unlocked state
- Topic:
  "/raw/esp8266/{chipid}/status" - will return locked state
  "/raw/esp8266/{chipid}/action" - will set servo position based on payload
- Payload:
  "lock" - will lock the door if the magnet is active
  "unlock" - unlock the door regardless of magnet state
*/

// MQTT
const char *mqtt_server = "192.168.2.244";
const uint16_t mqtt_port = 1883;
WiFiClient espClient;
PubSubClient client(espClient);
String callback_topic_base = "/raw/esp8266/" + String(ESP.getChipId());
String callback_topic_action = callback_topic_base + "/action";
String callback_topic_status = callback_topic_base + "/status";

// Servo
Servo servo;
int lock_status = -1;
String lock_status_message = "unlock";
int count_lock = 180;   // Degrees to lock
int count_unlock = 0;   // Unlock position

void processActionMessage(String payload){

  if (payload == "lock"){
    if (digitalRead(PIN_MAGNET) == 0){
      lock_status = count_lock;
      lock_status_message = payload;  // Save for status
    } else {
      Serial.println("Door is not closed. Cannot lock");
    }
  } else if (payload == "unlock"){
    lock_status = count_unlock;
    lock_status_message = payload;    // Save for status
  }
}

void callback(char *topic, byte *payload, unsigned int length)
{
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  String res = "";
  for (int i = 0; i < length; i++)
  {
    Serial.print((char)payload[i]);
    res += String((char) payload[i]);
  }
  Serial.println();
  String topicString = String(topic);

  if (topicString == callback_topic_action){
    processActionMessage(res);
  }

  if (topicString == callback_topic_status){
    client.publish((callback_topic_status+"/result").c_str(), lock_status_message.c_str());
  }

}

void reconnect()
{
  // Loop until we're reconnected
  while (!client.connected())
  {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str()))
    {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("/raw/esp8266/register", ("{ \"id\": \"" + String(ESP.getChipId()) + "\", \"ip\":\"" + WiFi.localIP().toString() + "\", \"type\":\"esp8266\", \"description\":\"door_servo\"}").c_str());

      // ... and resubscribe
      client.subscribe(callback_topic_action.c_str());
      client.subscribe(callback_topic_status.c_str());
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(115200);

  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
  //reset saved settings
  //wifiManager.resetSettings();
  wifiManager.autoConnect("DoorServoAP");

  //if you get here you have connected to the WiFi
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  //Setup MQTT callback
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  Serial.println("Listening to topic: " + callback_topic_base);

  //Magnet setup
  pinMode(PIN_MAGNET, INPUT_PULLUP);

  //Servo setup
  servo.attach(PIN_SERVO);
  if (digitalRead(PIN_MAGNET) == 0){
    servo.write(count_lock);
    lock_status_message = "lock";
  } else {
    servo.write(count_unlock);
    lock_status_message = "unlock";
  }
}

void loop()
{
  if (!client.connected())
  {
    reconnect();
  }
  client.loop();

  if (lock_status>-1){
    //Move to new value
    servo.write(lock_status);

    //Reset status
    lock_status = -1;
  }
}
